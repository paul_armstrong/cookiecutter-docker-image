======================
Cookiecutter PyPackage
======================

.. image:: https://gitlab.com/paul_armstrong/cookiecutter-docker-image/badges/main/pipeline.svg
        :target: https://gitlab.com/paul_armstrong/cookiecutter-docker-image/-/commits/main
        :alt: Documentation

Cookiecutter_ template for a Python package, specifically for repos in GitLab.
Handles pypi deployment using Docker GitLab CI pipelines.

* gitlab repo: https://gitlab.com/paul_armstrong/cookiecutter-docker-image
* Documentation: https://paul_armstrong.gitlab.io/cookiecutter-docker-image/
* Free software: BSD license

Features
--------

* Quick building for new standalone docker images to be deployed to the GitLab image registry using GitLab CI.

.. _Cookiecutter: https://gitlab.com/cookiecutter/cookiecutter

Quickstart
----------

Install the latest Cookiecutter if you haven't installed it yet (this requires
Cookiecutter 1.4.0 or higher)::

    pip install -U cookiecutter

Generate a Python package project using SSH::

    cookiecutter git@gitlab.com:paul_armstrong/cookiecutter-docker-image.git


OR using HTTPS::

    cookiecutter https://gitlab.com/paul_armstrong/cookiecutter-docker-image.git

Then:

* Create a repo and put it there.
* Add your requirements and setup your docker image, push to the new repository.
* After your first commit with a working set of requirements the docker image should begin building in the CI pipelines.
* After merging in your changes you can also now publish the image to the GitLab docker image registry using the GitLab tags.
* Create a new tag in the repository this will be published to the registry.

For more details, see the `cookiecutter-docker-image tutorial`_.

.. _`cookiecutter-docker-image tutorial`: https://cookiecutter-docker-image.readthedocs.io/en/latest/tutorial.html


Fork This / Create Your Own
~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you have differences in your preferred setup, I encourage you to fork this
to create your own version. Or create your own; it doesn't strictly have to
be a fork.

* Once you have your own version working, add it to the Similar Cookiecutter
  Templates list above with a brief description.

* It's up to you whether or not to rename your fork/own version. Do whatever
  you think sounds good.

.. _Tox: http://testrun.org/tox/
.. _Sphinx: http://sphinx-doc.org/
.. _`pyup.io`: https://pyup.io/
.. _bump2version: https://gitlab.com/c4urself/bump2version
.. _Punch: https://gitlab.com/lgiordani/punch
.. _Poetry: https://python-poetry.org/
.. _PyPi: https://pypi.python.org/pypi
.. _Mkdocs: https://pypi.org/project/mkdocs/


Forked from https://github.com/audreyfeldroy/cookiecutter-docker-image