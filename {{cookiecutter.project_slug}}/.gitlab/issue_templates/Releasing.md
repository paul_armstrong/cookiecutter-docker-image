### Releasing '{{cookiecutter.project_slug}}'

- [ ] Verify all necessary changes exist on your local master branch (i.e. `git pull origin master`)

### Announce
- [ ]
