{% set is_open_source = cookiecutter.open_source_license != 'Not open source' -%}
{% for _ in cookiecutter.project_name %}={% endfor %}
{{ cookiecutter.project_name }}
{% for _ in cookiecutter.project_name %}={% endfor %}


{% if cookiecutter.add_status_badges == 'y' %}

.. image:: https://img.shields.io/pypi/v/{{ cookiecutter.project_slug }}.svg
        :target: https://pypi.python.org/pypi/{{ cookiecutter.project_slug }}

.. image:: https://gitlab.com/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.project_slug | replace("_", "-") }}/badges/main/pipeline.svg
        :target: https://gitlab.com/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.project_slug | replace("_", "-") }}/-/commits/main
        :alt: Pipeline Status

.. image:: https://gitlab.com/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.project_slug | replace("_", "-") }}/badges/main/coverage.svg
        :target: https://gitlab.com/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.project_slug | replace("_", "-") }}/-/commits/main
        :alt: Coverage report

.. image:: https://gitlab.com/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.project_slug | replace("_", "-") }}/-/badges/release.svg
        :target: https://gitlab.com/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.project_slug | replace("_", "-") }}/-/releases
        :alt: Latest Release

.. image:: https://gitlab.com/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.project_slug | replace("_", "-") }}/-/badges/main/pipeline.svg
        :target: https://{{ cookiecutter.gitlab_username }}.gitlab.io/{{ cookiecutter.project_slug | replace("_", "-") }}
        :alt: Documentation

{% endif %}

{{ cookiecutter.project_short_description }}

{% if is_open_source %}
* Free software: {{ cookiecutter.open_source_license }}
{% endif %}

Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://gitlab.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://gitlab.com/audreyr/cookiecutter-pypackage

